<?php

use Illuminate\Database\Migrations\Migration;
use Database\Factories\UserFactory;


class InsertUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        for ($a=0; $a<100; $a++){
            DB::table('users')->insert(
                UserFactory::factoryForModel('User')->definition()
            );
        }
    }

}
